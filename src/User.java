
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class User {
    String userName;
    HashMap<Integer,Group> groupMap;
    ArrayList<Post> readPosts;
    
    @JsonCreator
    public User (@JsonProperty("userName") String userName,
                 @JsonProperty("groupMap") HashMap<Integer,Group> groupMap,
                 @JsonProperty("readPosts") ArrayList<Post> readPosts) {
        this.userName = userName;
        this.groupMap = groupMap;
        this.readPosts = readPosts;
    }
    
    public User (String userName) {
        this.userName = userName;
        groupMap = new HashMap();
        readPosts = new ArrayList();
    }

    public void setGroupMap(HashMap<Integer, Group> groupMap) {
        this.groupMap = groupMap;
    }    
    
    public ArrayList<Post> getReadPosts() {
        return readPosts;
    }

    public void setReadPosts(ArrayList<Post> readPosts) {
        this.readPosts = readPosts;
    }
    
    public void markRead(Post post){
        //post.setRead(true);
        for(Post p : readPosts){
            if(p.getAuthor().equals(post.getAuthor())){
                if(p.getBody().equals(post.getBody())){
                    if(p.getPostID() == post.getPostID()){
                        if(p.getTimestamp() == post.getTimestamp()){
                            if(p.getSubject().equals(post.getSubject())){
                                post.setRead(true);
                                readPosts.add(post);
                                updateReadStatus();
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void updateReadStatus(){
        Collection<Group> gList = groupMap.values();
        Post searchPost;
        for (int i = 0; i < readPosts.size(); i++){
            searchPost = readPosts.get(i);
            for (Group g : gList){
                for (Post p : g.getPostList()){
                    if (p.isRead() != searchPost.isRead())
                        p.setRead(!p.isRead());
                }
                
            }
        }
    }
    public boolean hasRead(Post post){
        return readPosts.contains(post);
    }
    
    public boolean isSubscribedTo(String gname){
        Group g;
        Set<Integer> keySet = groupMap.keySet();
        for(int i = 0; i < groupMap.size(); i++){
            g = groupMap.get((int)(keySet.toArray()[i]));
            if(g.groupName.toLowerCase().equals(gname)){
                return true;
            }
        }
        return false;
    }
    
    public void subscribeToGroup(Group g){
        groupMap.put(g.groupID, g);
    }

    public HashMap<Integer, Group> getGroupMap() {
        return groupMap;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    void unsubscribeFromGroup(int g) {
        groupMap.remove(g);
    }
}
