import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

/**
 *
 * @author haley.he
 */
public class Post {
    int postID;
    String author;
    String subject;
    String body;
    Date timestamp;
    boolean read;
    
    @JsonCreator
    public Post (@JsonProperty("postID") int postID,
                 @JsonProperty("author") String author,
                 @JsonProperty("subject") String subject,
                 @JsonProperty("body") String body,
                 @JsonProperty("timestamp") Date timestamp,
                 @JsonProperty("read") Boolean read) {
        this.postID = postID;
        this.author = author;
        this.subject = subject;
        this.body = body;
        this.timestamp = timestamp;
        this.read = read;
    }
    
    public Post (int postID, String author, String subject, String body, Date timestamp) {
        this.postID = postID;
        this.author = author;
        this.subject = subject;
        this.body = body;
        this.timestamp = timestamp;
        read = true;
    }
    
    public Post (int postID, String subject, Date timestamp) {
        this.postID = postID;
        this.author = author;
        this.subject = subject;
        this.body = body;
        this.timestamp = timestamp;
        read = true;
    }
    
    public int getPostID() {
        return postID;
    }

    public String getAuthor() {
        return author;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public Date getTimestamp() {
        return timestamp;
    }
    
    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
    
    
}
