
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Client {

    static User currentUser = null;

    static ArrayList<User> userArrayList = new ArrayList<User>();
    //static ArrayList<Group> groupArrayList = new ArrayList<Group>();

    static HashMap<Integer, Group> groupMap = new HashMap();

    static int serverPort = 9000;
    static String serverName = "localhost";
    static Scanner in;

    static ObjectMapper mapper;

    public static void main(String arg[]) {
        if (arg.length > 2) {
        serverName = arg[0];
        serverPort = Integer.parseInt(arg[1]);
        }
        in = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("> ");
                String input = in.nextLine();
                parseLine(input);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("That was not a valid input for the current mode.");
            }
        }
    }

    public static void printHelp() {
        System.out.println("View and post in discussion groups");
        System.out.println("Commands:");
        System.out.println("   login [username]");
        System.out.println("   logout");
        System.out.println("   help");
        System.out.println("   ag [N] - view N number of groups");
        System.out.println("      >s [...] - subscribe to ... group#/s");
        System.out.println("      >u [...] - unsubscribe from ... group#/s");
        System.out.println("      >n - view next set of N entries");
        System.out.println("      >q - quit ag mode");
        System.out.println("   sg [N] - view N number of subscribed groups");
        System.out.println("      >u [...] - unsubscribe from ... group#/s");
        System.out.println("      >n - view next set of N entries");
        System.out.println("      >q - quit ag mode");
        System.out.println("   rg gname [N] - view N number of posts in gname");
        System.out.println("      >[id] - view post #id");
        System.out.println("         >n - view next N lines of post");
        System.out.println("         >q - quit view post mode");
        System.out.println("      >r [#|#-#] - marks post/s as read");
        System.out.println("      >n - view next set of N posts");
        System.out.println("      >p - post to the group");
        System.out.println("      >q - quit rg mode");

    }

    public static void saveUserIntoJson() {
        /*Serge*/
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        Path relativePath = Paths.get(".\\resources\\users");

        // Checking if resources/users folders exist, if not, creates them
        if (!Files.exists(relativePath)) {
            relativePath.toFile().mkdirs();
        }

        try {
            // now that we have a username file, save currentUser to that file
            File saveFile = new File(relativePath.toString() + "\\" + currentUser.getUserName() + ".json");
            System.out.println("Trying to to save to: " + saveFile.toString());
            mapper.writeValue(saveFile, currentUser);

        } catch (IOException ex) {
            System.out.println("Failed to save groups to JSON.");
        }
    }

    // This method checks for any user data files. If they exist, loads them into
    // userArrayList so you could log in with previous data.
    public static void populateUserList() {
        mapper = new ObjectMapper();
        String currentDir = System.getProperty("user.dir") + "\\resources\\users";

        // Get a list of all existing user files
        File dirFile = new File(currentDir);
        File[] directory = dirFile.listFiles();

        if (directory != null && directory.length > 0) {
            userArrayList = new ArrayList<>();
            for (File userFile : directory) {
                User userToAdd;
                try {
                    userToAdd = mapper.readValue(userFile, User.class);
                    userArrayList.add(userToAdd);
                } catch (IOException ex) {
                    ex.printStackTrace();
                    System.out.println("Error reading in user file(s).");
                }
            }
        } else {
            System.out.println("There are no saved users.");
        }

    }

    public static void populateGroupList() {
        /*Serge*/
        groupMap = (HashMap<Integer, Group>) currentUser.getGroupMap().clone();
    }

    public static void login(String userName) {
        populateUserList();

        boolean loginSuccess = false;

        // If user exists in userArrayList (locally stored)
        for (User user : userArrayList) {
            if (user.getUserName().toLowerCase().equals(userName.toLowerCase())) {
                currentUser = user;
                populateGroupList();
                while (!loginSuccess) {
                    loginSuccess = loginRequest();
                }
                System.out.println("Welcome back " + userName + "!");
                //currentUser.updateReadStatus();
                return;
            }
        }

        // Else we must make a new user
        currentUser = new User(userName);
        userArrayList.add(currentUser);
        saveUserIntoJson();

        while (!loginSuccess) {
            loginSuccess = loginRequest();
        }
        System.out.println("User " + userName + " has been created.");

    }

    public static void parseLine(String input) {
        input = input.trim();
        String strSplit[] = input.split(" ", 2);
        switch (strSplit[0]) {
            case "help": //PRINT HELP
                printHelp();
                break;
            case "login": //LOGIN
                if (currentUser != null) {
                    System.out.println("Please logout before signing in again");
                } else if (strSplit.length > 1 && strSplit[1].trim().length() > 0) {
                    login(strSplit[1]);
                } else {
                    System.out.println("Please enter a user name.");
                }
                break;
            case "ag": // ALL GROUPS
                if (currentUser != null) {
                    enterAGMode(input);
                } else {
                    System.out.println("You must be logged in to execute this command.");
                }
                break;
            case "sg": //SUBSCRIBE TO GROUP
                if (currentUser != null) {
                    enterSGMode(input);
                } else {
                    System.out.println("You must be logged in to execute this command.");
                }
                break;
            case "rg": //READ GROUP
                if (currentUser != null) {
                    enterRGMode(input);
                } else {
                    System.out.println("You must be logged in to execute this command.");
                }
                break;
            case "logout": //LOGOUT
                logoutRequest();
                saveUserIntoJson();
                System.exit(0);
                break;
            default:
                System.out.println("Invalid input. Please try again.");
        }
    }

    public static void enterAGMode(String lineToProcess) {
        //SEND REQUEST
        boolean success = false;
        boolean postSuccess = false;
        while (!success) {
            success = makeAGRequest();
        }
        //WAIT FOR NEXT COMMAND
        String strSplitInit[] = lineToProcess.split(" ", 2);
        int agn;
        if (strSplitInit.length > 1) {
            agn = Integer.valueOf(strSplitInit[1]);
        } else {
            agn = 3; //CHANGE BACK TO 15
        }
        int scroll = 0;
        for (int i = 0; i < agn; i++) {
            Group g = groupMap.get(i + 1);
            if (g != null) {
                String s = " ";
                if (currentUser.getGroupMap().containsKey(i + 1)) {
                    s = "s";
                }
                System.out.println(g.getGroupID() + ". (" + s + ") " + g.getGroupName());
            }
        }
        scroll += agn;
        String input;
        while (true) {
            System.out.print("AG > ");
            input = in.nextLine();
            String strSplit[] = input.split(" ");
            switch (strSplit[0]) {
                case "s": //SUBSCRIBE
                    if (strSplit.length > 1) {
                        System.out.print("You have subscribed to ");
                        for (int i = 1; i < strSplit.length; i++) {
                            currentUser.subscribeToGroup(groupMap.get(Integer.valueOf(strSplit[i])));
                            System.out.print(groupMap.get(Integer.valueOf(strSplit[i])).groupName);
                            if (i < strSplit.length - 1) {
                                System.out.print(", ");
                            }
                        }
                        System.out.println();
                    } else {
                        System.out.println("Please enter number of the group you wish to subscribe.");
                    }
                    break;
                case "u": //UNSUBSCRIBE
                    if (strSplit.length > 1) {
                        System.out.print("You will be unsubscribed from ");
                        for (int i = 1; i < strSplit.length; i++) {
                            currentUser.unsubscribeFromGroup(groupMap.get(Integer.valueOf(strSplit[i])).groupID);
                            System.out.print(groupMap.get(Integer.valueOf(strSplit[i])).groupName);
                            if (i < strSplit.length - 1) {
                                System.out.print(", ");
                            }
                        }
                        System.out.println();
                    } else {
                        System.out.println("Please enter number of the group you wish to unsubscribe.");
                    }
                    break;
                case "n": //GET NEXT ELEMENTS
                    for (int i = 0; i < agn; i++) {
                        Group g = groupMap.get(scroll + i + 1);
                        if (g != null) {
                            String s = " ";
                            if (currentUser.groupMap.containsKey(scroll + i + 1)) {
                                s = "s";
                            }
                            System.out.println(g.getGroupID() + ". (" + s + ") " + g.getGroupName());
                        }
                    }
                    scroll += agn;
                    break;
                case "q": //QUIT MODE
                    saveUserIntoJson();
                    return;
                default:
                    System.out.println("Invalid input. Please try again.");
            }
            if (scroll > groupMap.size()) {
                return;
            }
        }
    }

    public static void enterSGMode(String lineToProcess) {
        //SEND REQUEST AND UPDATE ALL GROUP'S POST METADATA
        makeSGRequest();
        //WAIT FOR NEXT COMMAND
        String strSplitInit[] = lineToProcess.split(" ", 2);
        int agn;
        if (strSplitInit.length > 1) {
            agn = Integer.valueOf(strSplitInit[1]);
        } else {
            agn = 10;
        }
        int scroll = 0;
        for (int i = 0; i < agn; i++) {
            Group g = currentUser.getGroupMap().get(i + 1);
            if (g != null) {
                String s = " ";
                if (currentUser.getGroupMap().containsKey(i + 1)) {
                    s = "s";
                }
                System.out.println(g.getGroupID() + ". " + g.getPostList().size() + " " + g.getGroupName());

            }
        }
        scroll += agn;
        String input;
        while (true) {
            System.out.print("SG > ");
            input = in.nextLine();
            String strSplit[] = input.split(" ");
            switch (strSplit[0]) {
                case "u": //UNSUBSCRIBE
                    if (strSplit.length > 1) {
                        System.out.print("You will be unsubscribed from ");
                        for (int i = 1; i < strSplit.length; i++) {
                            currentUser.unsubscribeFromGroup(groupMap.get(Integer.valueOf(strSplit[i])).groupID);
                            System.out.print(groupMap.get(Integer.valueOf(strSplit[i])).groupName);
                            if (i < strSplit.length - 1) {
                                System.out.print(", ");
                            }
                        }
                        System.out.println();
                    } else {
                        System.out.println("Please enter number of the group you wish to unsubscribe.");
                    }
                    break;
                case "n": //GET NEXT ELEMENTS
                    // HERE IT MUST CHECK FOR ANY NEW POSTS FROM SERVER.
                    for (int i = 0; i < agn; i++) {
                        Group g = currentUser.getGroupMap().get(scroll + i + 1);
                        if (g != null) {
                            String s = " ";
                            if (currentUser.groupMap.containsKey(scroll + i + 1)) {
                                s = "s";
                            }
                            System.out.println(g.getGroupID() + ". " + g.getPostList().size() + " " + g.getGroupName());
                        }
                    }
                    scroll += agn;
                    break;
                case "q": //QUIT MODEl
                    saveUserIntoJson();
                    return;
                default:
                    System.out.println("Invalid input. Please try again.");
            }
            if (scroll > currentUser.getGroupMap().size()) {
                return;
            }
        }
    }

    public static Group getGroupFromName(String gname) {
        Group g;
        for (int key : groupMap.keySet()) {
            g = groupMap.get(key);
            if (g.getGroupName().toLowerCase().equals(gname.toLowerCase())) {
                return g;
            }
        }
        return null;
    }

    public static void enterRGMode(String lineToProcess) {
        populateGroupList();
        String strSplitInit[] = lineToProcess.split(" ", 3);
        if (strSplitInit.length < 2) {
            System.out.println("Invalid input. Please try again.");
            return;
        }
        String gName = strSplitInit[1];
        boolean found, success;
        found = success = false;
        Group currentG = null;
        //LOOK THROUGH ALL OF YOUR SUBSCRIBED GROUPS TO GET THE CORRECT ONE
        for (Group g : currentUser.getGroupMap().values()) {
            if (g.getGroupName().equals(gName)) {
                while (!success) {
                    success = makeRGRequest(g);
                }
                currentG = g;
                found = true;
                break;
            }
        }
        if (!found) {
            System.out.println("You are not subscribed to this group");
            return;
        }

        //WAIT FOR NEXT COMMAND
        String gname = null;
        if (strSplitInit.length > 1) {
            gname = strSplitInit[1];
        } else {
            System.out.println("Please specify a group to read.");
            return;
        }

        LinkedList<Post> linkedList = null;
        Group gr = null;
        if (currentUser.isSubscribedTo(gname.toLowerCase())) {
            if (getGroupFromName(gname.toLowerCase()) != null) {
                gr = getGroupFromName(gname.toLowerCase());
                linkedList = gr.getPostList();
            }
        } else {
            System.out.println("You are not subscribed to group " + gname + ".");
            return;
        }

        int agn;
        if (strSplitInit.length > 2) {
            agn = Integer.valueOf(strSplitInit[2]);
        } else {
            agn = 10;
        }
        int scroll = 0;
        for (int i = 0; i < agn; i++) {
            Post post = null;
            if (linkedList.size() > (scroll + i - 1)) {
                if (linkedList.size() > scroll + i) {
                    post = (Post) linkedList.get(scroll + i);
                } else {
                }
            }
            if (post != null) {
                String n = " ";
                if (!currentUser.hasRead(post)) {
                    n = "N";
                }
                //System.out.println(post.getPostID() + ". " + n + " " + post.getTimestamp() + " " + post.getSubject());
                System.out.println(post.getPostID() + ". " + n + " " + post.getTimestamp() + " " + post.getSubject());
            }
        }
        scroll += agn;
        String input;
        while (true) {
            System.out.print("RG > ");
            input = in.nextLine();
            String strSplit[] = input.split(" ");
            //CHECK TO SEE IF THE STRSPLIT[0] IS AN INTEGER. IF SO, WE ENTER THE ID MODE
            //ELSE SWITCH
            switch (strSplit[0]) {
                case "r": //SUBSCRIBE
                    if (strSplit.length > 1) {
                        if (strSplit[1].contains("-")) {
                            String strSplitR[] = strSplit[1].split("-");
                            if (strSplitR.length > 1) {
                                for (int i = Integer.valueOf(strSplitR[0]) - 1; i < (Integer.valueOf(strSplitR[1])); i++) {
                                    currentUser.markRead(linkedList.get(i));
                                    System.out.print(linkedList.get(i).getSubject());
                                    if (i < Integer.valueOf(strSplitR[1])) {
                                        System.out.print(", ");
                                    } else {
                                        System.out.print(" ");
                                    }
                                }
                                System.out.println(" marked as read.");
                                saveUserIntoJson();
                            }
                        } else {
                            int index = Integer.parseInt(strSplit[1]);
                            currentUser.markRead(linkedList.get(index));
                            //System.out.println(linkedList.get(index).getSubject() + " marked as read.");
                            saveUserIntoJson();
                        }
                    } else {
                        System.out.println("Please enter the number/s post to mark as read.");
                    }
                    //Need to do
                    break;
                case "n": //UNSUBSCRIBE
                    for (int i = 0; i < agn; i++) {
                        Post post = null;
                        if (linkedList.size() > (scroll + i - 1)) {
                            if (linkedList.size() > scroll + i) {
                                post = (Post) linkedList.get(scroll + i);
                            } else {
                            }
                        }
                        if (post != null) {
                            String n = " ";
                            //if(currentUser.groupMap.)
                            if (!currentUser.hasRead(post)) {
                                n = "N";
                            }
                            System.out.println(post.getPostID() + ". " + n + " " + post.getTimestamp() + " " + post.getSubject());
                        }
                    }
                    scroll += agn;
                    break;
                case "p": //GET NEXT ELEMENTS
                    System.out.println("Enter a subject: ");
                    String subject = in.nextLine();
                    System.out.println("Enter post content: ");
                    String body = in.nextLine();
                    Post newPost = new Post(gr.getPostList().size() + 1, currentUser.getUserName(), subject, body, new Date());
                    uploadPost(newPost, gr.groupID);
                    break;
                case "q": //QUIT MODE
                    saveUserIntoJson();
                    return;
                default:
                    enterIDMode(gr, input);
            }
            if (scroll > currentUser.getGroupMap().size()) {
                return;
            }
        }
    }

    public static void enterIDMode(Group g, String lineToProcess) {
        int postNumber = Integer.valueOf(lineToProcess);
        if (postNumber < 1 || postNumber > g.getPostList().size()) {
            System.out.println("Invalid request, please try again");
            enterRGMode("a " + g.groupName);
            return;
        }
        currentUser.markRead(g.getPostList().get(postNumber - 1));
        String postSplit[] = g.getPostList().get(postNumber - 1).getBody().split("\n");
        int agn = 3;
        int scroll = 0;
        for (int i = 0; i < agn; i++) {
            if (postSplit.length > (scroll + i - 1)) {
                if (postSplit.length > scroll + i) {
                    System.out.println(postSplit[scroll + i]);
                } else {
                }
            }
        }
        scroll += agn;

        //SEND REQUEST
        //UPDATE ALL GROUP DATA
        //WAIT FOR NEXT COMMAND
        String input;
        while (true) {
            System.out.print("RG/ID > ");
            input = in.nextLine();
            String strSplit[] = input.split(" ", 2);
            //CHECK TO SEE IF THE STRSPLIT[0] IS AN INTEGER. IF SO, WE ENTER THE INT MODE
            //ELSE SWITCH
            switch (strSplit[0]) {
                case "n": //NEXT
                    for (int i = 0; i < agn; i++) {
                        if (postSplit.length > (scroll + i - 1)) {
                            if (postSplit.length > scroll + i) {
                                System.out.println(postSplit[scroll + i]);
                            } else {
                            }
                        }
                    }
                    scroll += agn;
                    break;
                case "q": //QUIT MODE
                    return;
                default:
                    System.out.println("Invalid input. Please try again.");
            }
        }
    }

    public static void getNextN(String stringToRead) {

    }

    /**
     * Sends a login request to the server
     *
     * @return true if the connection was successful. Else return false
     */
    public static boolean loginRequest() {
        boolean success = false;
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        try {
            clientSocket = new Socket(serverName, serverPort);
            outToServer = clientSocket.getOutputStream();
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            //FORMAT THE OUTPUT MESSAGE
            PrintWriter pw = new PrintWriter(outToServer, false);
            //REQUEST TYPE
            pw.print("LOGIN \r\n");
            //DATA LINES
            pw.print("USER: " + currentUser.getUserName() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            ArrayList<String> inputArray = new ArrayList();
            String serverInput;
            //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF REQUEST
            while ((serverInput = inFromServer.readLine()) != null) {
                if (serverInput.length() == 0) {
                    break;
                }
                inputArray.add(serverInput);
            }
            //IF THE REQUEST WAS GARBLED, RETURN FALSE SO THE CLIENT WILL TRY TO CONNECT AGAIN
            if (inputArray.get(0).trim().equals("BADREQ")) {
                clientSocket.close();
                return false;
            }
            //CHECK TO SEE IF THE REQUEST WAS REC'D WITH A ECHO LOGIN
            if (inputArray.get(0).trim().equals("LOGIN")) {
                success = true;
            }
            clientSocket.close();
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
        return success;
    }

    /**
     * Sends a logout request to the server
     *
     * @return true if the connection was successful. Else return false
     */
    public static boolean logoutRequest() {
        boolean success = false;
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        try {
            clientSocket = new Socket(serverName, serverPort);
            outToServer = clientSocket.getOutputStream();
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            //FORMAT THE OUTPUT MESSAGE
            PrintWriter pw = new PrintWriter(outToServer, false);
            //REQUEST TYPE
            pw.print("LOGOUT \r\n");
            //DATA LINES
            pw.print("USER: " + currentUser.getUserName() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            ArrayList<String> inputArray = new ArrayList();
            String serverInput;
            //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF REQUEST
            while ((serverInput = inFromServer.readLine()) != null) {
                if (serverInput.length() == 0) {
                    break;
                }
                inputArray.add(serverInput);
            }
            //IF THE REQUEST WAS GARBLED, RETURN FALSE SO THE CLIENT WILL TRY TO CONNECT AGAIN
            if (inputArray.get(0).trim().equals("BADREQ")) {
                clientSocket.close();
                return false;
            }
            //CHECK TO SEE IF THE REQUEST WAS REC'D WITH A ECHO LOGIN
            if (inputArray.get(0).trim().equals("LOGOUT")) {
                success = true;
            }
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
        return success;
    }

    /**
     * Sends a request to the server to get all of the group data and stores it
     * into a global ArrayList
     *
     * @return true if the connection was successful. Else return false
     */
    public static boolean makeAGRequest() {
        boolean success = false;
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        try {
            clientSocket = new Socket(serverName, serverPort);
            outToServer = clientSocket.getOutputStream();
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            PrintWriter pw = new PrintWriter(outToServer, false);
            //REQUEST TYPE LINE
            pw.print("AGREQ\r\n");
            //DATA LINES
            pw.print("USER: " + currentUser.getUserName() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            ArrayList<String> inputArray = new ArrayList();
            ArrayList<Integer> recvdGroupIDs = new ArrayList();
            String serverInput;

            //CLEAR OUT THE CURRENT AG LIST AND CREATEA A NEW ONE
            groupMap.clear();
            do {
                inputArray.clear();
                //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF THE MESSAGE BLOCK
                while ((serverInput = inFromServer.readLine()) != null) {
                    if (serverInput.length() == 0) {
                        break;
                    }
                    inputArray.add(serverInput);
                }
                //IF THE REQUEST WAS GARBLED, RETURN FALSE SO THE CLIENT WILL TRY TO CONNECT AGAIN
                if (inputArray.get(0).trim().equals("BADREQ")) {
                    clientSocket.close();
                    return false;
                }
                //IF YOU GET AN AG_RESPONSE MESSAGE. ADD THE GROUP
                if (inputArray.get(0).trim().equals("AGRESP") && inputArray.size() >= 3) {
                    String gID = inputArray.get(1);
                    gID = gID.substring(gID.indexOf(":") + 1).trim();
                    String gName = inputArray.get(2);
                    gName = gName.substring(gName.indexOf(":") + 1).trim();
                    groupMap.put(Integer.parseInt(gID), new Group(Integer.parseInt(gID), gName));
                    // ADD THE GROUPID TO THE RECV'D ARRAYLIST FOR THE ACK
                    recvdGroupIDs.add(Integer.parseInt(gID));

                }
                //IF YOU GET AN AG_DONE MESSAGE. NO MORE GROUPS TO READ
            } while (!inputArray.get(0).trim().equals("AGDONE"));

            //SEND ACK MESSAGE
            pw.print("AGACK\r\n");
            //DATA LINES: SEND TOTAL NUMBER OF GROUPS REC'D
            pw.print("REC-GROUP-NUM: " + recvdGroupIDs.size() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            //WAIT for ACK/NACK OF AGNACK
            serverInput = inFromServer.readLine();
            //IF AN ACK MESSAGE WAS REC'D. AG HAS SUCCESSFULLY BEEN COMPLETEED. NACK MEANS RESEND AGREQ8EST
            if (serverInput.trim().equals("AGACK")) {
                success = true;
            }
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
        return success;
    }

    /**
     * Sends a request to the server to get all of the subscribed group's post
     * meta-data.
     */
    public static void makeSGRequest() {
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        PrintWriter pw;
        HashMap<Integer, Group> groupMap = currentUser.getGroupMap();
        Set<Integer> subscriptionKey = groupMap.keySet();
        try {
            boolean success;
            //FOR EACH SUBSCRIBED GROUP, CONNECT TO THE SERVER AND ASK OR THE METADATA
            for (int i : subscriptionKey) {
                clientSocket = new Socket(serverName, serverPort);
                outToServer = clientSocket.getOutputStream();
                inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                pw = new PrintWriter(outToServer, false);
                success = false;
                //KEEP TRYING TO GET THIS GROUP'S POST METADATA UNTIL YOU GET AN ACK FROM THE SEREVER
                while (!success) {
                    //REQUEST AND GET THE POST META DATA
                    int postRecv = processPostMeta(clientSocket, groupMap.get(i));
                    //IF CLIENT GETS A BAD REQUEST, TRY TO CONNECT AGAIN
                    if (postRecv == -1) {
                        success = false;
                    } else {
                        //SEND ACK MESSAGE
                        pw.print("SGACK\r\n");
                        //DATA LINES: SEND TOTAL NUMBER OF GROUPS REC'D
                        pw.print("REC-POST-NUM: " + postRecv + "\r\n");
                        pw.print("\r\n");
                        pw.flush();
                        String serverInput = inFromServer.readLine();
                        //IF AN ACK MESSAGE WAS REC'D. AG HAS SUCCESSFULLY BEEN COMPLETEED. NACK MEANS RESEND AGREQ8EST
                        if (serverInput.trim().equals("SGACK")) {
                            success = true;
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
    }

    /**
     * Helper method for makeSGRequest(). Gets a subscribed group's post
     * meta-data
     *
     * @param clientSocket the open socket through which the SG request is made
     * @param groupID the subscribed group the client wants meta-data from
     * @returns the number of post received from the server. Used for ACK
     */
    private static int processPostMeta(Socket clientSocket, Group group) throws IOException {
        OutputStream outToServer = clientSocket.getOutputStream();
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(outToServer, false);

        //REQUEST TYPE LINE
        pw.print("SGREQ\r\n");
        //DATA LINES
        pw.print("USER: " + currentUser.getUserName() + "\r\n");
        pw.print("GROUP-ID: " + group.getGroupID() + "\r\n");
        pw.print("\r\n");
        pw.flush();

        LinkedList<Post> postList = group.getPostList();
        ArrayList<String> inputArray = new ArrayList();
        String serverInput;
        int numPostRecv = 0;
        //KEEP READING POST METADATA INPUT FROM THE SERVER
        do {
            inputArray.clear();
            //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF THE MESSAGE BLOCK
            while ((serverInput = inFromServer.readLine()) != null) {
                if (serverInput.length() == 0) {
                    break;
                }
                inputArray.add(serverInput);
            }
            //GET RESPONSE MESSAGE
            if (inputArray.get(0).trim().equals("BADREQ")) {
                return -1;
            }
            if (inputArray.get(0).trim().equals("SGRESP")) {
                numPostRecv++;
                String pID = inputArray.get(1);
                pID = pID.substring(pID.indexOf(":") + 1).trim();
                int postID = Integer.parseInt(pID);
                boolean found = false;
//CHECK TO SEE IF THIS POST IS ALREADY IN THE LIST. IF IT IS, YOU DONT NEED TO ADD IT
                for (int j = 0; j < postList.size(); j++) {
                    if (postList.get(j).getPostID() == postID) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    //IF NOT IN THE LIST, ADD POST TO THE LSIT
                    String pName = inputArray.get(2);
                    pName = pName.substring(pName.indexOf(":") + 1).trim();
                    String pDateString = inputArray.get(3); //SENT FROM SERVER AS A TIMESTAMP SINCE LONG
                    long pTime = Long.parseLong(pDateString.substring(pDateString.indexOf(":") + 1).trim());
                    Post newPost = new Post(postID, pName, new Date(pTime));
                    postList.addFirst(newPost);
                }
            }
            //IF YOU GET AN AG_DONE MESSAGE. NO MORE GROUPS TO READ
        } while (!inputArray.get(0).trim().equals("SGDONE"));
        return numPostRecv;
    }

    /**
     * Sends a request to the server to get the full post data for a specified
     * subscribed group
     *
     * @param currentGroup the subscribed group from which the client wants to
     * read post from
     * @return true if the connection was successful. Else return false
     */
    private static boolean makeRGRequest(Group currentGroup) {
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        PrintWriter pw;
        boolean success = false;
        LinkedList<Post> postList = currentGroup.getPostList();
        try {
            clientSocket = new Socket(serverName, serverPort);
            outToServer = clientSocket.getOutputStream();
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            pw = new PrintWriter(outToServer, false);
            //REQUEST TYPE 
            pw.print("RGREQ\r\n");
            //DATA LINES
            pw.print("USER: " + currentUser.getUserName() + "\r\n");
            pw.print("GROUP-NAME: " + currentGroup.getGroupID() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            ArrayList<String> inputArray = new ArrayList();
            String pDateString, pSubject, pAuthor, pBody, serverInput;
            int postID;
            long timestamp;
            boolean found;
            int numPostRecv = 0;
            //KEEP GETTING REPONSE DATA UNTIL A RGDONE RESPONSE IS SENT
            do {
                inputArray.clear();
                //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF THE MESSAGE BLOCK
                while ((serverInput = inFromServer.readLine()) != null) {
                    if (serverInput.length() == 0) {
                        break;
                    }
                    inputArray.add(serverInput);
                }
                //IF THE REQUEST WAS GARBLED, RETURN FALSE SO THE CLIENT WILL TRY TO CONNECT AGAIN
                if (inputArray.get(0).trim().equals("BADREQ")) {
                    clientSocket.close();
                    return false;
                }
                //READ THE MESSAGE BODY
                //IF YOU GET AN AG_RESPONSE MESSAGE. ADD THE GROUP
                if (inputArray.get(0).trim().equals("RGRESP")) {
                    //RESET THE FOUND BOOLEANS
                    found = false;
                    numPostRecv++;
                    //PARSE THE RESPONSE
                    postID = Integer.parseInt(getLineData(inputArray.get(1)));
                    pAuthor = getLineData(inputArray.get(3));
                    pBody = getLineData(inputArray.get(5));
                    //FIND THE POST IN TE GROUP
                    for (int j = 0; j < postList.size(); j++) {
                        Post p = postList.get(j);
                        if (p.getPostID() == postID) {
                            p.setAuthor(pAuthor);
                            p.setBody(pBody);
                            found = true;
                            break;
                        }
                    }
                    //IF THE POST IS NOT IN THE GROUP, ADD IT TO THE GROUP
                    if (!found) {
                        pSubject = getLineData(inputArray.get(2));
                        //pDateString = getLineData(inputArray.get(4));
                        timestamp = Long.parseLong(getLineData(inputArray.get(4)));
                        Post newPost = new Post(postID, pSubject, new Date(timestamp));
                        newPost.setAuthor(pAuthor);
                        newPost.setBody(pBody);
                        postList.addFirst(newPost);
                    }
                }
                //IF YOU GET AN AG_DONE MESSAGE. NO MORE GROUPS TO READ
            } while (!inputArray.get(0).trim().equals("RGDONE"));

            //SEND ACK MESSAGE
            pw.print("RGACK" + "\r\n");
            //DATA LINES: SEND TOTAL NUMBER OF GROUPS REC'D
            pw.print("REC-POST-NUM: " + numPostRecv + "\r\n");
            pw.print("\r\n");
            pw.flush();
            //WAIT TO GET AN ACK BACK FROM THE SEREVER
            serverInput = inFromServer.readLine();
            if (getLineData(serverInput).equals("RGACK")) {
                success = true;
            }
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
        return success;
    }

    /**
     * Takes a new post and sends it to the server to be uploaded
     *
     * @param newPost the new post that will be uploaded to the server
     * @param groupID the subscribed group to which the new post will be
     * uploaded
     * @return true if the connection was successful. Else return false
     */
    public static boolean uploadPost(Post newPost, int groupID) {
        boolean success = false;
        Socket clientSocket = null;
        OutputStream outToServer;
        BufferedReader inFromServer;
        PrintWriter pw;
        try {
            clientSocket = new Socket(serverName, serverPort);
            outToServer = clientSocket.getOutputStream();
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            pw = new PrintWriter(outToServer, false);
            //REQUEST TO ADD NEW POST TO SERVER
            pw.print("POST\r\n");
            //POST DATA
            pw.print("USER: " + currentUser.getUserName() + "\r\n");
            pw.print("GROUP-NAME: " + groupID + "\r\n");
            pw.print("POST-ID: " + newPost.getPostID() + "\r\n");
            pw.print("POST-SUBJECT: " + newPost.getSubject() + "\r\n");
            pw.print("POST-AUTHOR: " + newPost.getAuthor() + "\r\n");
            pw.print("POST-DATE: " + newPost.getTimestamp().getTime() + "\r\n");
            pw.print("POST-BODY: " + newPost.getBody() + "\r\n");
            pw.print("\r\n");
            pw.flush();

            //WAIT FOR ACK RESPONSE
            ArrayList<String> responseArray = new ArrayList();
            String responseInput;
            while ((responseInput = inFromServer.readLine()) != null) {
                if (responseInput.length() == 0) {
                    break;
                }
                responseArray.add(responseInput);
            }

            //CHECK FOR POST_ACK
            String recString = responseArray.get(1);
            //OBTAIN THE ACK CHECK FOR THE 
            int numRec = Integer.parseInt(getLineData(recString));
            int messageLength = newPost.getBody().length() + newPost.getSubject().length();
            //IF ACK IS GOOD, SEND ACK BACK
            if (responseArray.get(0).trim().equals("POSTACK") && numRec == messageLength) {
                success = true;
            } else if (responseArray.get(0).trim().equals("BADREQ")) {
                success = false;
            }
        } catch (IOException ex) {
            System.out.println("Unable to connect to server. Trying again...");
        } finally {
            try {
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                System.out.println("Failed to close socket");
            }
        }
        return success;
    }

    //Helper method for parsing data
    public static String getLineData(String input) {
        return input.substring(input.indexOf(":") + 1).trim();
    }

    //helper method for getting post by id
    private static Post getPostById(int id, LinkedList<Post> l) {
        Post foundPost = null;
        for (Post p : l) {
            if (p.getPostID() == id) {
                foundPost = p;
            }
        }
        return foundPost;
    }
}
