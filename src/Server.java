
import java.util.HashMap;
import java.util.Set;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author haley.he
 * @author serge.crane
 */
public class Server implements Runnable {

    static int lisPort = 9000;
    static LinkedList<String> activeUsers;
    static ArrayList<Group> groups;
    Socket connectSocket;
    static ObjectMapper mapper;

    Server(Socket csocket) {
        this.connectSocket = csocket;
    }

    //Constantly looping for client request. If there is a request, spawn a new thread for the client
    public static void main(String[] args) {
        activeUsers = new LinkedList();
        groups = new ArrayList();
        try {
            initGroups();

            ServerSocket welcomeSocket = new ServerSocket(lisPort);
            while (true) {
                Socket connectionSocket = welcomeSocket.accept();
                new Thread(new Server(connectionSocket)).start();
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        DataOutputStream outToClient = null;
        try {
            //CREATE INPUT STREAM
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSocket.getInputStream()));
            //CREATE OUTPUT STREAM
            outToClient = new DataOutputStream(connectSocket.getOutputStream());
            ArrayList<String> inputArray = new ArrayList();
            String input;
            //READ UNTIL THERE IS AN EMPTY LINE. THIS SIGNALS END OF REQUEST
            while ((input = inFromClient.readLine()) != null) {
                if (input.length() == 0) {
                    break;
                }
                //System.out.println(input);
                inputArray.add(input);
            }   //PROCESS THE REQUEST
            parseRequest(inputArray);
            connectSocket.close();
        } catch (IOException ex) {
            System.out.println("Connection to client lost.");
        } finally {
            try {
                if (connectSocket != null) {
                    connectSocket.close();
                }
                outToClient.close();
            } catch (IOException ex) {
                System.out.println("Failed to close connection");
            }
        }
    }

    // Checks the status of the request and forwards the request to the proper method to be processed
    private void parseRequest(ArrayList<String> inputArray) throws IOException {
        //FIRST ELEMENT IN THE ARRAY IS THE REQUEST TYPE
        String requestType = inputArray.get(0).trim();
        String userID = inputArray.get(1);
        //CALL THE APPROPRIATE METHOD BASED ON THE REQUEST TYPE 
        switch (requestType) {
            case "LOGIN":
                addActiveUser(userID);
                break;
            case "LOGOUT":
                removeActiveUser(userID);
                break;
            case "AGREQ":
                processAGRequest();
                break;
            case "SGREQ":
                processSGRequest(inputArray);
                break;
            case "RGREQ":
                processRGRequest(inputArray);
                break;
            case "POST":
                processPostRequest(inputArray);
                updateServerJSON();
                break;
            default:
                markAsBadInput();
        }
    }

    //Adds a client to the ArrayList of active clients and responss to client with echo LOGIN
    private void addActiveUser(String userIDField) throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        String userID = userIDField.substring(userIDField.indexOf(':') + 1);
        userID = userID.trim();
        synchronized (this) {
            activeUsers.add(userID);
        }
        //ACKNOWLEDGE LOGIN BY ECHOING LOGIN REQUEST
        outToClient.writeBytes("LOGIN");
        System.out.println(userID + " has joined. Total active users: " + activeUsers.size());
    }

    //Removes a client to the ArrayList of active clients and responss to client with echo LOGOUT
    private void removeActiveUser(String userIDField) throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        String userID = userIDField.substring(userIDField.indexOf(':') + 1);
        userID = userID.trim();
        synchronized (this) {
            activeUsers.remove(userID);
        }
        //ACKNOWLEDGE LOGIN BY ECHOING LOGOUT REQUEST
        outToClient.writeBytes("LOGOUT");
        System.out.println(userID + " has left. Total active users: " + activeUsers.size());
    }

    private void markAsBadInput() throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        PrintWriter pw = new PrintWriter(outToClient, false);
        pw.print("BADREQ\r\n");
        pw.print("\r\n");
        pw.flush();
    }

    //SENDS ALL OF THE GROUP INFORMATION TO THE CLIENT
    private void processAGRequest() throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(outToClient, false);
        //SEND REACH GROUP OBJECT.
        for (int i = 0; i < groups.size(); i++) {
            pw.print("AGRESP\r\n");
            //DATA
            synchronized (this) {
                //System.out.println("Sending group " + groups.get(i).getGroupName());
                pw.print("GROUPID: " + groups.get(i).getGroupID() + "\r\n");
                pw.print("GROUPNAME: " + groups.get(i).getGroupName() + "\r\n");
            }
            pw.print("\r\n");
            pw.flush();
        }

        //SEND FINISHED STATUS
        pw.print("AGDONE\r\n");
        pw.print("\r\n");
        pw.flush();
        //WAIT FOR AGACK RESPONSE
        ArrayList<String> inputArray = new ArrayList();
        String input;
        while ((input = inFromClient.readLine()) != null) {
            if (input.length() == 0) {
                break;
            }
            inputArray.add(input);
        }

        //CHECK FOR AG_ACK
        String recString = inputArray.get(1);
        //OBTAIN THE NUMBER OF GROUPS RECV'D FROM THE ACK MESSAGE
        int numRec = Integer.parseInt(recString.substring(recString.indexOf(":") + 1).trim());
        //IF ACK IS GOOD, SEND ACK BACK
        if (inputArray.get(0).trim().equals("AGACK") && numRec == groups.size()) {
            pw.write("AGACK\r\n");
            pw.write("\r\n");
            pw.flush();
            // NUM OF GROUPS REC'D DONT MATCH, SEND MACK SO CLIENT WILL RESEND REQUEST
        } else {
            pw.write("AGNACK\r\n");
            pw.write("\r\n");
            pw.flush();
        }
    }

    //SEND ALL GROUP'S POST META-DATA TO THE CLIENT
    private void processSGRequest(ArrayList<String> inputArray) throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(outToClient, false);
        //PARSE THE INPUT FROM THE CLIENT
        String gID = getLineData(inputArray.get(2));
        //Group currentGroup = groups.get(Integer.parseInt(gID) - 1);
        Group currentGroup = getGroupById(Integer.parseInt(gID));
        LinkedList<Post> postList = currentGroup.getPostList();

        //SEND EACH OF THE POST TO THE CLIENT
        for (int i = 0; i < postList.size(); i++) {
            pw.print("SGRESP\r\n");
            synchronized (this) {
                pw.print("POST-ID: " + postList.get(i).getPostID() + "\r\n");
                pw.print("POST-SUBJECT: " + postList.get(i).getSubject() + "\r\n");
                pw.print("POST-DATE: " + postList.get(i).getTimestamp().getTime() + "\r\n");
            }
            pw.print("\r\n");
            pw.flush();
        }
        //SEND FINISHED STATUS
        pw.print("SGDONE\r\n");
        pw.print("\r\n");
        pw.flush();

        //WAIT FOR ACK RESPONSE
        ArrayList<String> responseArray = new ArrayList();
        String responseInput;
        while ((responseInput = inFromClient.readLine()) != null) {
            if (responseInput.length() == 0) {
                break;
            }
            responseArray.add(responseInput);
        }

        //CHECK FOR AG_ACK
        String recString = responseArray.get(1);
        //OBTAIN THE NUMBER OF GROUPS RECV'D FROM THE ACK MESSAGE
        int numRec = Integer.parseInt(getLineData(recString));
        //IF ACK IS GOOD, SEND ACK BACK
        if (responseArray.get(0).trim().equals("SGACK") && numRec == postList.size()) {
            pw.write("SGACK\r\n");
            pw.write("\r\n");
            pw.flush();
            // NUM OF GROUPS REC'D DONT MATCH, SEND MACK SO CLIENT WILL RESEND REQUEST
        } else {
            pw.write("SGNACK\r\n");
            pw.write("\r\n");
            pw.flush();
        }
    }

    //SENDS ALL OF A GROUP'S FULL POST DATA TO THE CLIENT
    private void processRGRequest(ArrayList<String> inputArray) throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(outToClient, false);
        //PARSE THE INPUT FROM THE CLIENT
        String gID = getLineData(inputArray.get(2));
        Group currentGroup = null;
        for (Group g : groups) {
            if (g.getGroupID() == Integer.parseInt(gID)) {
                currentGroup = g;
            }
        }

        LinkedList<Post> postList = currentGroup.getPostList();

        //SEND EACH OF THE POST TO THE CLIENT
        for (int i = 0; i < postList.size(); i++) {
            pw.print("RGRESP\r\n");
            synchronized (this) {
                pw.print("POST-ID: " + postList.get(i).getPostID() + "\r\n");
                pw.print("POST-SUBJECT: " + postList.get(i).getSubject() + "\r\n");
                pw.print("POST-AUTHOR: " + postList.get(i).getAuthor() + "\r\n");
                pw.print("POST-DATE: " + postList.get(i).getTimestamp().getTime() + "\r\n");
                pw.print("POST-BODY: " + postList.get(i).getBody() + "\r\n");

            }
            pw.print("\r\n");
            pw.flush();
        }
        //SEND FINISHED STATUS
        pw.print("RGDONE\r\n");
        pw.print("\r\n");
        pw.flush();

        //WAIT FOR ACK RESPONSE
        ArrayList<String> responseArray = new ArrayList();
        String responseInput;
        while ((responseInput = inFromClient.readLine()) != null) {
            if (responseInput.length() == 0) {
                break;
            }
            responseArray.add(responseInput);
        }

        //CHECK FOR AG_ACK
        String recString = responseArray.get(1);
        //OBTAIN THE NUMBER OF GROUPS RECV'D FROM THE ACK MESSAGE
        int numRec = Integer.parseInt(getLineData(recString));
        //IF ACK IS GOOD, SEND ACK BACK
        if (responseArray.get(0).trim().equals("RGACK") && numRec == postList.size()) {
            pw.write("RGACK\r\n");
            pw.write("\r\n");
            pw.flush();
            // NUM OF GROUPS REC'D DONT MATCH, SEND MACK SO CLIENT WILL RESEND REQUEST
        } else {
            pw.write("RGNACK\r\n");
            pw.write("\r\n");
            pw.flush();
        }
    }

    //ADDS A POST TO THE SPECIFIED GROUP IF IT IS NOT ALREADY IN THERE
    private void processPostRequest(ArrayList<String> inputArray) throws IOException {
        DataOutputStream outToClient = new DataOutputStream(connectSocket.getOutputStream());
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectSocket.getInputStream()));
        PrintWriter pw = new PrintWriter(outToClient, false);

        //GET THE GROUP THAT IS POST IS UPLOADED TO
        int gID = Integer.parseInt(getLineData(inputArray.get(2)));
        //Group g = groups.get(gID - 1);
        Group g = getGroupById(gID);
        // PARSE THE INPUT FOR THET POST DATA
        String pSubject, pAuthor, pBody;
        int postID = Integer.parseInt(getLineData(inputArray.get(3)));
        pSubject = getLineData(inputArray.get(4));
        pAuthor = getLineData(inputArray.get(5));
        long timestamp = Long.parseLong(getLineData(inputArray.get(6)));
        pBody = getLineData(inputArray.get(7));
        //RECREATE THE POST
        Post newPost = new Post(postID, pSubject, new Date(timestamp));
        newPost.setAuthor(pAuthor);
        newPost.setBody(pBody);
        //CHECK TO SEE IF THE POT IS ALREADY IN THE GROUP. IF IT IN ALREADY IN LIST, DO NOTHING AND ACK
        LinkedList<Post> postList = g.getPostList();
        boolean found = false;
        for (int i = 0; i < postList.size(); i++) {
            if (newPost.getPostID() == postList.get(i).getPostID()) {
                found = true;
                break;
            }
        }
        //IF THIS IS A NEW POST, ADD THE POST TO THE POST LIST OF THE SPECIFIED GROUP 
        if (!found) {
            postList.addFirst(newPost);
            System.out.println("Added post #" + newPost.getPostID() + " to the server");
        }

        int messageLen = pSubject.length() + pBody.length();
        //SEND ACK MESSAGE
        pw.print("POSTACK" + "\r\n");
        //DATA LINES: SEND TOTAL NUMBER OF GROUPS REC'D
        pw.print("REC-POST-LEN: " + messageLen + "\r\n");
        pw.print("\r\n");
        pw.flush();

    }

    //HEPER METHOD FOR PARSING DATA LINES
    public static String getLineData(String input) {
        return input.substring(input.indexOf(":") + 1).trim();
    }

    // This method reads the group data from the JSON file and populates the groups ArrayList
    public static void initGroups() {
        mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String currentDir = System.getProperty("user.dir") + "\\resources\\groups";

        // Get a list of all existing group files
        File dirFile = new File(currentDir);
        File[] directory = dirFile.listFiles();

        // Iterate over every listing, create Group object and add it to groups
        if (directory != null && directory.length > 0) {
            groups = new ArrayList();
            for (File groupFile : directory) {
                try {
                    Group groupToAdd = mapper.readValue(groupFile, Group.class);
                    groups.add(groupToAdd);
                } catch (IOException ex) {
                    System.out.println("Error reading in group file.");
                }
            }
        } else {
            System.out.println("No group data found. Creating fake data.");
            initFakeGroups();
        }
    }

    // This method initializes fake group data when JSON file does not exist yet.
    public static void initFakeGroups() {
        Post p1, p2, p3;

        Group g1 = new Group(1, "CSE 310 - Discussion");
        p1 = new Post(1, "Serge", "Welcome", "Welcome to 310 Discussion posts! \n\n\n Enjoy your stay.", new Date());
        p2 = new Post(2, "Steve", "This class is awesome", "Studying networks brings me heartfelt joy.", new Date());
        p3 = new Post(3, "Haley", "I like programming servers", "Why are servers so fun?", new Date());
        g1.addPost(p1);
        g1.addPost(p2);
        g1.addPost(p3);
        groups.add(g1);
        Group g2 = new Group(2, "Politics");
        p1 = new Post(1, "Serge", "Trump", "He is our president.", new Date());
        p2 = new Post(2, "Steve", "Politics", "Take POL102.", new Date());
        p3 = new Post(3, "Haley", "Hello", "I love politics", new Date());
        g2.addPost(p1);
        g2.addPost(p2);
        g2.addPost(p3);
        groups.add(g2);
        Group g3 = new Group(3, "Technology");
        p1 = new Post(1, "Serge", "Welcome", "Tell me about the newest technologies", new Date());
        p2 = new Post(2, "Steve", "Next Big Thing", "What is the next big thing?.", new Date());
        p3 = new Post(3, "Haley", "Androids", "Android master race", new Date());
        g3.addPost(p1);
        g3.addPost(p2);
        g3.addPost(p3);
        groups.add(g3);
        Group g4 = new Group(4, "Sports");
        p1 = new Post(1, "Serge", "Basketball", "Basketball is the best.", new Date());
        p2 = new Post(2, "Steve", "Favorite team", "Talk about your favorite team here.", new Date());
        p3 = new Post(3, "Haley", "Why am I here?", "I hate sports", new Date());
        g4.addPost(p1);
        g4.addPost(p2);
        g4.addPost(p3);
        groups.add(g4);
        Group g5 = new Group(5, "Movies");
        p1 = new Post(1, "Serge", "Frozen", "Just let it go....", new Date());
        p2 = new Post(2, "Steve", "What to watch?", "Give me movie suggestions", new Date());
        p3 = new Post(3, "Haley", "Moana", "You need to go watch it", new Date());
        g5.addPost(p1);
        g5.addPost(p2);
        g5.addPost(p3);
        groups.add(g5);
        Group g6 = new Group(6, "Music");
        p1 = new Post(1, "Serge", "Classical Music", "I love Mozart.", new Date());
        p2 = new Post(2, "Steve", "Indie Music", "Please gie me some suggestions", new Date());
        p3 = new Post(3, "Haley", "Favorite Song", "WHat are your favorite songs?", new Date());
        g6.addPost(p1);
        g6.addPost(p2);
        g6.addPost(p3);
        groups.add(g6);
        Group g7 = new Group(7, "Jokes");
        p1 = new Post(1, "Serge", "Puns", "What did the sea say to the sand? Nothing, it simply waved..", new Date());
        p2 = new Post(2, "Steve", "Just a joke", "I once worked in a bank, but then I lost interest.", new Date());
        p3 = new Post(3, "Haley", "Bad Joke", "This joke was so bad. I am not going to post it", new Date());
        g7.addPost(p1);
        g7.addPost(p2);
        g7.addPost(p3);
        groups.add(g7);
        Group g8 = new Group(8, "Games");
        p1 = new Post(1, "Serge", "XBOX", "Talk about your favorite XBOX games", new Date());
        p2 = new Post(2, "Steve", "Game development", "How do i get into it?", new Date());
        p3 = new Post(3, "Haley", "Mobile Games", "They waste too much of myy time", new Date());
        g8.addPost(p1);
        g8.addPost(p2);
        g8.addPost(p3);
        groups.add(g8);
        Group g9 = new Group(9, "Programming");
        p1 = new Post(1, "Serge", "CSE310", "Networking is the best class", new Date());
        p2 = new Post(2, "Steve", "CSE373", "Algorithms are important.", new Date());
        p3 = new Post(3, "Haley", "CSE320", "Hopefully I wont have to retake this class", new Date());
        g9.addPost(p1);
        g9.addPost(p2);
        g9.addPost(p3);
        groups.add(g9);
        Group g10 = new Group(10, "Fitness");
        p1 = new Post(1, "Serge", "Yoga", "Everyone should try doing yoga.", new Date());
        p2 = new Post(2, "Steve", "Weight lifting", "Do you even lift?", new Date());
        p3 = new Post(3, "Haley", "Motivation", "Help movitivate me to go to the gym.", new Date());
        g10.addPost(p1);
        g10.addPost(p2);
        g10.addPost(p3);
        groups.add(g10);
        Group g11 = new Group(11, "Art");
        p1 = new Post(1, "Serge", "Baroque", "Teach me about Baroque art", new Date());
        p2 = new Post(2, "Steve", "Renaissance", "The best period of art", new Date());
        p3 = new Post(3, "Haley", "Modern art", "I don't understand...", new Date());
        g11.addPost(p1);
        g11.addPost(p2);
        g11.addPost(p3);
        groups.add(g11);
        Group g12 = new Group(12, "Football");
        p1 = new Post(1, "Serge", "Favorite Team", "Who is your favorite team?", new Date());
        p2 = new Post(2, "Steve", "Game tonight", "Who is watching the game tonight.", new Date());
        p3 = new Post(3, "Haley", "Wrong group", "Please delete this post", new Date());
        g12.addPost(p1);
        g12.addPost(p2);
        g12.addPost(p3);
        groups.add(g12);
        Group g13 = new Group(13, "Soccer");
        p1 = new Post(1, "Serge", "Favorite player", "Who is your favorite plater on team X", new Date());
        p2 = new Post(2, "Steve", "Soccer", "Yay soccer", new Date());
        p3 = new Post(3, "Haley", "Looking for lessons", "Teach me please", new Date());
        g13.addPost(p1);
        g13.addPost(p2);
        g13.addPost(p3);
        groups.add(g13);
        Group g14 = new Group(14, "Volleyball");
        p1 = new Post(1, "Serge", "Places to Go Play", "Place Y is the best. Every go play there", new Date());
        p2 = new Post(2, "Steve", "Looking for teammates", "Anyone wants to form a team?", new Date());
        p3 = new Post(3, "Haley", "Olympics", "Talk about the games here", new Date());
        g14.addPost(p1);
        g14.addPost(p2);
        g14.addPost(p3);
        groups.add(g14);
        Group g15 = new Group(15, "YouTube");
        p1 = new Post(1, "Serge", "Favorite gamer", "Give me some good gaming channels.", new Date());
        p2 = new Post(2, "Steve", "Educational", "I learn a lot from YouTube.", new Date());
        p3 = new Post(3, "Haley", "YouTube is evil", "Too much procrastination..,", new Date());
        g15.addPost(p1);
        g15.addPost(p2);
        g15.addPost(p3);
        groups.add(g15);
        Group g16 = new Group(16, "Random");
        p1 = new Post(1, "Serge", "Books", "I enjoy reading novels", new Date());
        p2 = new Post(2, "Steve", "Winter break", "Can't wait for the break to come.", new Date());
        p3 = new Post(3, "Haley", "Happy birthday", "Hahppy birthday to you my friend", new Date());
        g16.addPost(p1);
        g16.addPost(p2);
        g16.addPost(p3);
        groups.add(g16);

        // Save the new fake groups into the groups.json file.
        updateServerJSON();
    }

    //SERGE: METHOD IS CALLED IN ParseRequst() in the post section of the switch
    //This method will take the updated versio of the static groups ArrayList and store it into a JSON file
    public static void updateServerJSON() {
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        Path relativePath = Paths.get(".\\resources\\groups");

        // Checking if resources/groups folders exist, if not, creates them
        if (!Files.exists(relativePath)) {
            relativePath.toFile().mkdirs();
        }

        try {
            File saveFile;
            for (Group g : groups) {
                saveFile = new File(relativePath.toString() + "\\" + g.getGroupID() + ".json");
                //System.out.println("Trying to to save to: " + saveFile.toString());
                mapper.writeValue(saveFile, g);
            }

        } catch (IOException ex) {
            System.out.println("Failed to save groups to JSON.");
        }
    }

    private Group getGroupById(int id) {
        Group currentGroup = null;
        for (Group g : groups) {
            if (g.getGroupID() == id) {
                currentGroup = g;
            }
        }
        return currentGroup;
    }
}
