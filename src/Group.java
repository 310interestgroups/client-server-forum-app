import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author haley.he
 */
public class Group {
    int groupID;
    String groupName;
    LinkedList<Post> postList;
    
    @JsonCreator   
    public Group (@JsonProperty("groupID") int groupID,
                  @JsonProperty("groupName") String groupName,
                  @JsonProperty("postList") LinkedList<Post> postList) {
        this.groupID = groupID;
        this.groupName = groupName;
        this.postList = postList;
    }
    
    public Group (int groupID, String groupName) {
        this.groupID = groupID;
        this.groupName = groupName;
        postList = new LinkedList();
    }
    
    public Group (int groupID, String groupName, Date lastModified) {
        this.groupID = groupID;
        this.groupName = groupName;
        postList = new LinkedList();

    }
    
    public void addPost(Post post){
        postList.add(post);
    }

    public LinkedList<Post> getPostList() {
        return postList;
    }

    public void setPostList(LinkedList<Post> postList) {
        this.postList = postList;
    }   
    
    public int getGroupID() {
        return groupID;
    }

    public String getGroupName() {
        return groupName;
    }
}
