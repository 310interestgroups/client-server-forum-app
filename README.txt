Welcome to our discussion group application!
By Serge Crane, Steven Marturano, Haley He


You must start the server first. It will begin listening at port 9000. If there are peripheral files in its source directory,
it will create and initialize them.

Start the client, it accepts two input parameters: hostname, port

java Client hostname 9000

After that you may use the application. The following are the available commands. Please type them exactly as you see,
as there are edge cases where erroneous input will crash the application.

Commands:
   login [username]
   logout
   help
   ag [N] - view N number of groups
      >s [...] - subscribe to ... group#/s
      >u [...] - unsubscribe from ... group#/s
      >n - view next set of N entries
      >q - quit ag mode
   sg [N] - view N number of subscribed groups
      >u [...] - unsubscribe from ... group#/s
      >n - view next set of N entries
      >q - quit ag mode
   rg gname [N] - view N number of posts in gname
      >[id] - view post #id
         >n - view next N lines of post
         >q - quit view post mode
      >r [#|#-#] - marks post/s as read
      >n - view next set of N posts
      >p - post to the group
      >q - quit rg mode